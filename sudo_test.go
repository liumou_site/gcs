package gcs

import (
	"fmt"
	"testing"

	"gitee.com/liumou_site/logger"
)

// TestSudo 测试以sudo权限执行命令的功能。
// 该测试以sudo权限安装然后卸载一个软件包，以验证sudo功能是否正常工作。
func TestSudo(t *testing.T) {
	// 实例化构造函数
	sudo := NewSudo("1")
	// 开启调试信息
	sudo.PrintInfo = true
	sudo.Realtime = true
	sudo.Debug = true
	// 执行sudo命令(此时会使用第一步传入的密码作为sudo密码去生成命令并执行)
	// 如果已经是root权限，则直接执行当前命令
	packages := "vsftpd"
	sudo.RunSudo("apt install -y ", packages)
	if sudo.Err == nil {
		logger.Info("安装成功: ", packages)
	} else {
		logger.Error("安装失败: ", packages)
		logger.Error(sudo.Err)
		fmt.Println(sudo.Strings)
	}
	// 关闭实时输出，准备执行下一个命令
	sudo.Realtime = false
	sudo.RunSudo("apt purge -y ", packages)
	if sudo.Err == nil {
		logger.Info("Uninstall OK: ", packages)
	} else {
		logger.Error("Uninstall Failed ", packages)
		logger.Error(sudo.Err)
		fmt.Println(sudo.Strings)
	}
}

// TestSudoLine 测试 Sudo 功能的函数
// 该函数演示了如何使用 Sudo 类型来执行需要 sudo 权限的命令
func TestSudoLine(t *testing.T) {
	// 实例化构造函数
	sudo := NewSudo("1")
	// 开启调试信息
	sudo.PrintInfo = true
	sudo.Realtime = true
	sudo.Debug = true
	// 执行sudo命令(此时会使用第一步传入的密码作为sudo密码去生成命令并执行)
	// 如果已经是root权限，则直接执行当前命令
	sudo.RunScriptSudo("docker ps -a | grep cf282df5be23")
	if sudo.Err == nil {
		logger.Info("执行成功")
		fmt.Println(sudo.Strings)
		// 解析并打印特定行列的数据
		sudo.Line(2).Column(3, " ")
		fmt.Println("第一行第二列:", sudo.Strings)
	} else {
		logger.Error("执行失败")
		logger.Error(sudo.Err)
	}
	// 执行不需要脚本模式的sudo命令
	sudo.RunSudo("docker ps -a")
	fmt.Println(sudo.Strings)
	// 执行带有多个参数的sudo命令
	sudo.RunSudo("docker ps", "-aq")
	fmt.Println(sudo.Strings)
}

// 测试超时命令
//func TestTimeOut(t *testing.T) {
//	// 实例化构造函数
//	sudo := NewShell("1", false)
//	// 开启调试信息
//	sudo.PrintInfo = true
//	// 执行sudo命令(此时会使用第一步传入的密码作为sudo密码去生成命令并执行)
//	// 如果已经是root权限，则直接执行当前命令
//	_, err := sudo.SudoTimeOut("ping baidu.com -c 30", 3)
//	if err == nil {
//		logger.Info("连接成功")
//	} else {
//		logger.Error("连接失败")
//	}
//}
