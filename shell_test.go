package gcs

import (
	"fmt"
	"strings"
	"testing"

	"gitee.com/liumou_site/logger"
)

func TestApiShell_RunTerminal(t *testing.T) {
	ter := NewShell()
	ter.Debug = true
	ter.RunTerminal("ping -c 20 baidu.com")
}

// 测试命令执行
func TestShell(t *testing.T) {
	// 执行成功
	cc := NewShell()
	cc.BlackHole = true
	cc.Ignore = true
	cc.RunShell("ls", "/ss/")
	logger.Debug("退出代码: ", cc.ExitCode)
	if cc.Err == nil {
		logger.Info("执行成功")
		logger.Debug("文件列表如下")
		fmt.Println(cc.Strings)
	} else {
		logger.Error("执行失败")
		fmt.Println(cc.Err)
	}
	cc.Grep("md")
	fmt.Println("r", cc.Strings)
}

// 测试脚本执行
func TestShellScript(t *testing.T) {
	// 执行成功
	cc := NewShell()
	cc.RunScript("ls / | grep etc")
	logger.Debug("退出代码: ", cc.ExitCode)
	if cc.Err == nil {
		logger.Info("执行成功")
		logger.Debug("文件列表如下")
		fmt.Println(cc.Strings)
	} else {
		logger.Error("执行失败")
		fmt.Println(cc.Err)
	}
	cc.RunScript("ls /home/")
	fmt.Println(cc.Strings)
}

func TestDpkg(t *testing.T) {
	c := NewShell()
	c.RunShell("dpkg -l")
	if c.Err != nil {
		logger.Error(c.Err.Error())
	}
	c.Grep("docker").Grep("ce").Line(1).Column(3, " ")
	fmt.Println(c.Strings)
	cmd := NewShell()
	cmd.RunScript("route | grep default | awk '{print $8}'")
	if cmd.Err != nil {
		logger.Error(cmd.Err.Error())
	}
	cmd.Strings = strings.Split(cmd.Strings, "\n")[0]
	fmt.Println(cmd.Strings)
	fmt.Println(len(cmd.Strings))
}
func TestCheckCmd(t *testing.T) {
	ls_ := CheckCmd("dir")
	if ls_ {
		logger.Info("存在")
	} else {
		logger.Error("不存在命令")
	}
	ll_ := CheckCmd("dirs")
	if ll_ {
		logger.Info("存在")
	} else {
		logger.Error("不存在命令")
	}
}

func TestApiShell_AwkCol(t *testing.T) {
	a := NewShell()
	a.RunShell("route")
	a.Grep("default")
	a.AwkCol("3")
	fmt.Println(a.Strings)
}

func TestApiShell_Column(t *testing.T) {
	a := NewShell()
	a.Debug = true
	a.RunShell("route")
	a.Grep("default")
	a.Column(3, " ")
	fmt.Println(a.Strings)
}

// 测试命令执行
//func TestGetOut(t *testing.T) {
//	// 执行成功
//	cc := NewShell()
//	c := strings.Split("ls /home/liumou@ls /home/lll@lw /home/@ls -la", "@")
//	for _, s := range c {
//		out, err := cc.RunErrOut(s)
//		if err == nil {
//			logger.Info("执行成功")
//		} else {
//			logger.Error("执行失败")
//		}
//		fmt.Println(out)
//	}
//}

// // 测试实时刷新命令
// func TestRe(t *testing.T) {
// 	// 执行成功
// 	c := strings.Split("ls /home/liumou@lss", "@")
// 	for _, s := range c {
// 		code := RunSystem(s)
// 		if code != 0 {
// 			logger.Error("执行失败")
// 		} else {
// 			logger.Info("执行成功")
// 		}
// 		logger.Debug("退出代码: ", code)
// 	}
// }

// 测试超时命令
func TestApiShell_RunTimeout(t *testing.T) {
	// 实例化构造函数
	shell := NewShell()
	// 开启调试信息
	shell.Debug = true
	shell.PrintErr = true
	// 执行sudo命令(此时会使用第一步传入的密码作为sudo密码去生成命令并执行)
	// 如果已经是root权限，则直接执行当前命令
	shell.RunTimeout(10, "ping baidu.com")
	if shell.Err == nil {
		logger.Info("连接成功")
	} else {
		logger.Error("连接失败")
	}
	fmt.Println(shell.Err)
}
