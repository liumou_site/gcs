package gcs

import (
	"gitee.com/liumou_site/gf"
	"gitee.com/liumou_site/logger"
	"os"
)

var logs *logger.LocalLogger // 日志打印

// init
//
//	@Description: 初始化日志模块
func init() {
	logs = logger.NewLogger(3)
	logs.Version = Version()
	logs.Modular = "gcs"
}

// createScript 创建并写入一个 Bash 脚本文件。
//
// 参数:
//   - text: string - 包含脚本的主要内容。
//   - file: string - 指定脚本文件的保存路径。
//   - quit: bool - 布尔值，决定脚本是否在执行完毕后直接退出而不提示用户。
//
// 返回值:
//   - error - 如果操作成功则返回 nil，否则返回相应的错误。
func createScript(text, file string, quit bool) error {
	// 创建或打开指定的脚本文件，并清空其内容。
	f := gf.NewFile(file)
	f.DeleteFile()

	// 根据 quit 参数设置脚本的退出行为。
	qr := "code=$?\nread -p Close\nexit $code"
	if quit {
		qr = "echo $?"
	}

	// 构建完整的脚本内容。
	txt := "#!/bin/bash\n" + text + "\n" + qr
	f.Echo(txt)

	// 设置文件权限为可执行。
	if f.Err == nil {
		err := os.Chmod(file, 0777)
		return err
	} else {
		// 记录创建脚本过程中遇到的错误。
		logger.Error("createScript:", file, " Err:", f.Err)
	}

	// 返回文件操作过程中的错误信息。
	return f.Err
}
