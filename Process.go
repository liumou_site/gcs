package gcs

// ProcessIsRunning 检查指定命令的进程是否正在运行。
// 参数:
//
//	command (string): 需要检测的命令名称。
//
// 返回值:
//
//	bool: 如果进程正在运行，则返回true；否则返回false。
func (api *ApiShell) ProcessIsRunning(command string) bool {
	// 如果启用了调试模式，则打印调试信息。
	if api.Debug {
		logs.Debug("正在通过命令名称检测进程运行状态: ", command)
	}

	// 执行命令来检查进程状态。
	api.RunShell("ps -C ", command)

	// 如果没有错误发生并且退出代码为0，则表示进程正在运行。
	if api.Err == nil {
		if api.ExitCode == 0 {
			return true
		}
	}

	// 打印调试信息，显示退出代码。
	logs.Debug("Exit Code: %d", api.ExitCode)

	// 如果退出代码不为0，则表示进程未运行，返回false。
	return false
}
