# gcs

## 介绍

使用Golang编写的系统命令执行模块,实现类似`Python3`的`os.System`、`subprocess.getstatusoutput`及`subprocess.getoutput`

## 安装教程

执行下面的命令

```shell
go get -u gitee.com/liumou_site/gcs
```

效果如下

```shell
PS D:\data\git\Go\glbm> go  get -u gitee.com/liumou_site/gcs   
go: downloading gitee.com/liumou_site/gcs v1.1.0
go: downloading golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
go: added gitee.com/liumou_site/gcs v1.1.0
PS D:\data\git\Go\glbm> 
```

## 使用说明


```go
package demo

import (
	"fmt"
	"gitee.com/liumou_site/gcs"
	"gitee.com/liumou_site/logger"
)

func GcsShell(cmd string) {
	shell := gcs.NewShell()
	shell.RunSudo(cmd)
	if shell.Err != nil {
		logger.Error("执行失败")
	} else {
		logger.Info("执行成功")
		fmt.Println(shell.Strings)
	}
}

func GcsSudo(cmd, password string)  {
	s := gcs.NewSudo(password)
	s.Realtime = true // 开启实时打印
	s.RunSudo(cmd)
	s.Line(1).Column(5, " ")
	fmt.Println(s.Strings)
}
```

## 常用参数说明


| 参数名称 | 值 | 作用|
|--|--|--|
| Realtime | bool | 实时打印命令执行反馈信息|
| Strings | String| 存储命令执行反馈文本信息|

更多参数请查阅`api.go`文件

# 问题反馈

点击链接加入QQ群聊【[坐公交也用券](https://jq.qq.com/?_wv=1027&k=FEeLQ6tz)】