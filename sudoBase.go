package gcs

import (
	"strings"

	"gitee.com/liumou_site/gbm"
)

// Grep 方法用于在 ApiSudo 结构体的 Strings 字段中查找包含指定字符串的行。
// 它利用关联的 Grep 函数来执行查找操作，并更新 ApiSudo 结构体的字段以反映查找结果。
// 参数:
//
//	match - 要查找的字符串。
//
// 返回值:
//
//	*ApiSudo - 返回指向 ApiSudo 实例的指针，以便进行链式调用。
func (api *ApiSudo) Grep(match string) *ApiSudo {
	// 将 ApiSudo 的 Strings 字段赋值给 gfs.Text，准备进行查找操作。
	api.gfs.Text = api.Strings
	// 调用 gfs 的 Grep 方法执行实际的字符串查找操作。
	api.gfs.Grep(match)
	// 将查找结果更新回 ApiSudo 的 Strings 字段。
	api.Strings = api.gfs.Text
	// 捕获并保存 gfs.Grep 操作中可能产生的错误。
	api.Err = api.gfs.Err
	// 将查找结果按行分割成字符串切片。
	r := strings.Split(api.Strings, "\n")
	// 使用 gbm.SliceRemoveNull 移除切片中的空字符串元素。
	api.Slice = gbm.SliceRemoveNull(r)
	// 再次捕获并保存可能产生的错误。
	api.Err = api.gfs.Err
	// 返回指向 ApiSudo 实例的指针，以便进行链式调用。
	return api
}

// Echo 打印命令执行反馈信息
//func (api *ApiSudo) Echo() {
//	fmt.Println(api.Strings)
//}

//// Line 截取指定行
//func (api *ApiSudo) Line(n int) *ApiSudo {
//	api.gfs.Text = api.Strings
//	api.gfs.Line(n)
//	api.Strings = api.gfs.Text
//	api.Err = api.gfs.Err
//	return api
//}
//
//// Column 截取指定列
//func (api *ApiSudo) Column(col int, sep string) *ApiSudo {
//	api.gfs.Text = api.Strings
//	api.gfs.Column(col, sep)
//	api.Strings = api.gfs.Text
//	api.Err = api.gfs.Err
//	return api
//}
