package gcs

import (
	"gitee.com/liumou_site/gf"
)

// ApiShell
// @Description: 定义命令的结构体
type ApiShell struct {
	PrintErr     bool          // 打印详细错误信息(默认: true)
	PrintInfo    bool          // 是否打印详细信息(默认: false)
	Debug        bool          // 是否打印调试信息(默认: false)
	home         string        // 当前用户家目录(自动判断)
	user         string        // 当前用户名(自动判断)
	uid          int           // 当前用户uid(自动判断)
	Realtime     bool          // 是否开启实时打印信息
	Result       bool          // 是否执行成功
	Text         string        // 需要执行的命令
	OsType       string        // 系统类型
	args         []string      // 执行参数
	Strings      string        // 输出结果
	ExitCode     int           // 命令执行退出代码
	Err          error         // 错误
	gfs          gf.ReadScreen // 文本处理实例
	Slice        []string      // 数据切片
	Script       string        // 脚本文件
	ScriptMode   bool          // 脚本文件
	cmdName      string        // 最终命令,例如: /bin/bash
	argRun       []string      // 最终命令参数
	Terminal     string        // 使用的终端命令
	TerminalArg  string        // 使用的终端参数
	TerminalBool bool          // 是否可以使用终端
	BlackHole    bool          // 是否使用黑洞模式(忽略错误信息)
	Ignore       bool          // 是否忽略标准输出
}

// ApiSudo 定义命令的结构体
type ApiSudo struct {
	SudoPath string // sudo命令绝对路径
	Password string // sudo权限使用的密码
	isRoot   bool   // 是否属于root用户(自动判断)
	ApiShell        // 继承Shell实例
}

type ApiHandle struct {
	Strings string   // 处理结果
	Err     error    // 错误
	Slice   []string // 切片
}
