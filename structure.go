package gcs

import (
	"fmt"
	"log"
	"os/exec"
	"os/user"
	"path"
	"runtime"
	"strconv"

	"gitee.com/liumou_site/gf"
)

// NewShell 命令实例构造函数 Realtime 是否开启实时打印数据
//
//	@Description:
//	@return *ApiShell
func NewShell() *ApiShell {
	shell := new(ApiShell)
	// 系统信息
	shell.OsType = runtime.GOOS // 获取当前系统类型
	get, username, uid, uHome := GetUserInfo(false)
	if get {
		shell.home = uHome
		shell.user = username
		shell.uid = uid
	} else {
		shell.home = "None"
		shell.user = "None"
		shell.uid = 10000
	}
	shell.Script = path.Join(uHome, ".gcs.sh") // 创建临时脚本文件
	shell.Debug = false                        // 调试开关
	shell.PrintErr = true                      // 错误信息打印开关
	shell.PrintInfo = false                    // 详细信息打印开关
	shell.Realtime = false                     // 实时打印信息
	shell.BlackHole = false                    // 不开启黑洞模式
	shell.Ignore = false                       // 不开启标准输出屏蔽
	shell.gfs = *gf.NewReadFile("r")
	terList := map[string]string{"konsole": "-e", "deepin-terminal": "-C", "mate-terminal": "-e"}
	shell.TerminalBool = false
	for i, v := range terList {
		shell.RunShell("which", i)
		if shell.Err == nil {
			shell.Terminal = i
			shell.TerminalArg = v
			shell.TerminalBool = true
		}
	}
	//fmt.Println(shell.Terminal)
	//fmt.Println(shell.TerminalArg)
	return shell
}

// NewSudo 命令实例构造函数, 当不需要执行sudo命令的时候直接传入任意字符串给password即可
//
//	@Description:
//	@param password 主机密码
//	@return *ApiSudo
func NewSudo(password string) *ApiSudo {
	sudo := new(ApiSudo)
	// 系统信息
	sudo.Password = password + "\n"
	sudo.OsType = runtime.GOOS // 获取当前系统类型
	get, username, uid, uHome := GetUserInfo(false)
	if get {
		sudo.home = uHome
		sudo.user = username
		sudo.uid = uid
	} else {
		sudo.home = "None"
		sudo.user = "None"
		sudo.uid = 10000
	}
	var sp string
	if sudo.user == "root" {
		sudo.isRoot = true
		sudo.SudoPath = sp
	} else {
		if sudo.OsType == "linux" {
			// 获取sudo路径
			sp, err := exec.LookPath("sudo")
			if err != nil {
				errs := fmt.Errorf("找不到sudo程序")
				fmt.Println(errs)
				sudo.SudoPath = "sudo"
			} else {
				sudo.SudoPath = sp
			}
		}
	}
	//logger.Debug(sudo.SudoPath)
	sudo.Script = path.Join(uHome, ".gcs.sh") // 创建临时脚本文件
	sudo.Debug = false                        // 调试开关
	sudo.PrintErr = true                      // 错误信息打印开关
	sudo.PrintInfo = false                    // 详细信息打印开关
	sudo.Realtime = false                     // 实时打印信息
	sudo.BlackHole = false                    // 不开启黑洞模式
	sudo.Ignore = false                       // 不开启标准输出屏蔽
	sudo.gfs = *gf.NewReadFile("r")
	return sudo
}

// GetUserInfo 获取用户名,用户uid,用户家目录
//
//	@Description:
//	@param display 是否显示详细信息
//	@return ok 获取结果
//	@return username 用户名
//	@return userid 用户ID
//	@return UserHome 用户主目录
func GetUserInfo(display bool) (ok bool, username string, userid int, UserHome string) {
	var id int
	// fmt.Println(path)
	currentUser, err := user.Current()
	if err != nil {
		log.Fatalf(err.Error())
		return false, "None", 10000, "None"
	}
	name := currentUser.Name
	id, err = strconv.Atoi(currentUser.Uid)
	if err != nil {
		id = 10000
	}
	home := currentUser.HomeDir
	if display {
		fmt.Println("UserName is: ", name)
		fmt.Println("UserId is: ", id)
		fmt.Println("UserHome : ", home)
	}
	return true, name, id, home
}

// CheckCmd 检查命令是否存在
//
//	@Description:
//	@param cmd 需要检查的命令
//	@return bool 是否存在
func CheckCmd(cmd string) bool {
	// 通过此函数可以从path变量查询命令是否存在，返回命令绝对路径和查找结果
	_, err := exec.LookPath(cmd)
	return err == nil
}
