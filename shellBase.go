package gcs

import (
	"fmt"
	"gitee.com/liumou_site/gbm"
	"gitee.com/liumou_site/gf"
	"path"
	"strings"
)

// Grep 方法用于在 ApiShell 实例的字符串数组中搜索匹配指定文本的行。
// 它利用 api.gfs 的 Grep 方法进行实际的搜索操作，并更新 ApiShell 实例的状态以反映搜索结果。
// 参数:
//
//	match - 要搜索的字符串。
//
// 返回值:
//
//	*ApiShell - 返回当前的 ApiShell 实例，用于支持链式调用。
func (api *ApiShell) Grep(match string) *ApiShell {
	// 将当前字符串数组赋值给 gfs.Text，以便进行搜索。
	api.gfs.Text = api.Strings
	// 调用 gfs 的 Grep 方法进行搜索。
	api.gfs.Grep(match)
	// 将搜索结果赋值回 api.Strings。
	api.Strings = api.gfs.Text
	// 捕获搜索过程中可能发生的错误。
	api.Err = api.gfs.Err

	// 将搜索结果按行分割成字符串数组。
	r := strings.Split(api.Strings, "\n")
	// 移除数组中为空的元素，避免空指针或无效数据。
	api.Slice = gbm.SliceRemoveNull(r)
	// 再次捕获可能发生的错误。
	api.Err = api.gfs.Err

	// 返回当前的 ApiShell 实例，使链式调用成为可能。
	return api
}

// Echo 方法用于打印 ApiShell 实例中的 Strings 属性。
// 该方法没有输入参数，也不返回任何值。
// 主要用途是展示或调试 Strings 属性的内容。
func (api *ApiShell) Echo() {
	fmt.Println(api.Strings)
}

// AwkCol 提取并打印指定列的数据。
// 该方法使用 awk 工具从临时文件中提取指定的列，并将结果打印出来。
// 参数:
//
//	col (string): 需要提取的列号，例如 "1" 表示第一列。
func (api *ApiShell) AwkCol(col string) {
	// 构建临时文件路径
	file := path.Join(api.home, "awk.tmp")

	// 创建或打开临时文件
	f := gf.NewFile(file)

	// 将数据写入临时文件
	f.Echo(api.Strings)

	// 构建 awk 命令，用于提取指定列
	cmd := "awk '{print $" + col + "}' " + file

	// 打印 awk 命令以便调试
	fmt.Println(cmd)

	// 执行 awk 命令并获取结果
	api.RunScript(cmd)
}

// Line 方法用于处理文本数据，使其以指定的行数显示
// 参数 n 指定文本应显示的行数
// 该方法返回 ApiShell 类型的指针，使得方法调用可以链式方式进行
func (api *ApiShell) Line(n int) *ApiShell {
	// 将当前文本数据设置为 ApiShell 结构体中的 gfs.Text 字段
	api.gfs.Text = api.Strings
	// 调用 gfs 的 Line 方法，根据传入的行数参数处理文本
	api.gfs.Line(n)
	// 将处理后的文本数据更新回 ApiShell 结构体中的 Strings 字段
	api.Strings = api.gfs.Text
	// 将 gfs 的错误信息更新到 ApiShell 结构体中的 Err 字段
	api.Err = api.gfs.Err
	// 返回处理后的 ApiShell 结构体指针
	return api
}

// Column 方法按列解析字符串数据，并使用指定的分隔符。
// 该方法接收两参数：col(int) 和 sep(string)，分别表示列号和分隔符。
// 它首先将字符串按行分割，然后调用 gfs 的 Column 方法进行列解析，
// 最后更新 api 的字符串数据和错误信息，并返回 api 实例。
func (api *ApiShell) Column(col int, sep string) *ApiShell {
	// 如果启用了调试模式，则打印当前字符串数据。
	if api.Debug {
		logs.Debug(api.Strings)
	}

	// 将字符串按行分割成切片，并移除空行。
	sp := strings.Split(api.Strings, "\n")
	sp = gbm.SliceRemoveNull(sp)

	// 打印处理后的切片及其长度，以便调试。
	fmt.Println(sp)
	fmt.Println(len(sp))

	// 将原始字符串数据赋给 gfs 的 Text 属性，以便后续处理。
	api.gfs.Text = api.Strings

	// 调用 gfs 的 Column 方法，按列解析数据。
	api.gfs.Column(col, sep)

	// 更新 api 的字符串数据和错误信息。
	api.Strings = api.gfs.Text
	api.Err = api.gfs.Err

	// 返回 api 实例，支持链式调用。
	return api
}
