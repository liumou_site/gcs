package gcs

import "testing"

// TestApiShell_ProcessIsRunning 测试 ProcessIsRunning 函数是否能正确判断进程是否运行。
// 该函数没有输入参数和返回值。
// 此测试用例使用 "dpkg" 作为示例进程名称来检查该进程是否正在运行。
// 如果进程正在运行，将记录信息日志；如果未运行，则记录调试日志。
// 这个测试函数的目的是验证 ProcessIsRunning 函数在实际应用中的行为是否符合预期。
func TestApiShell_ProcessIsRunning(t *testing.T) {
	// 创建一个 Shell 实例。
	p := NewShell()

	// 检查 "dpkg" 进程是否正在运行。
	if p.ProcessIsRunning("dpkg") {
		// 如果进程正在运行，记录信息日志。
		logs.Info("程序运行中")
	} else {
		// 如果进程未运行，记录调试日志。
		logs.Debug("程序未运行")
	}
}
